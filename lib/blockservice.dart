import 'dart:convert';
import 'dart:io';
import 'dart:async';
import 'package:http/http.dart' as http;

String url = "https://api-carwash.herokuapp.com/data";
var headers = {HttpHeaders.contentTypeHeader: 'application/json'};
List datos;

Future<List<List<Resume>>> obtenerdatospost(Map data) async {


  Map data = {
    "idwash": "T09u5gV8IQeKs61CGTxd",
    "idser": ['JeuK3FtwHmo1eYRL4rA2', 'PNPDmuUKdAqDsolPKkJL'],
    "dateRequested": "13-07-2020 08: 00: 00"
  };

  /* Map data = {
     "idwash": userServices.data["cid"],
     "idser": servicios,
     "dateRequested": "13-07-2020 08: 00: 00"
              };*/

  final posdata =
      await http.post(url, headers: headers, body: json.encode(data));

  if (posdata.statusCode == 200) {
    var responseDecode = json.decode(posdata.body) as List;
    var blockmap = responseDecode.map((e) => Block.fromJson(e)).toList();
    // var datos = itemToJson(blockmap[0]);
    // print(datos);

    var resumemap = blockmap.map((e) => (e.resume)).toList();


    return resumemap;
  } else {
    throw Exception("Fallo la solicitud ");
  }
}

/*.forEach((keys, value) {

    }); */

class ListBlock {
  List<Block> block;

  ListBlock({this.block});

  factory ListBlock.fromJson(List<dynamic> json) {
    List<Block> bloques = json.map((i) => Block.fromJson(i)).toList();
    return new ListBlock(block: bloques);
  }
}

class Block {
  String idService;
  int idBlock;
  int duration;
  KeyValueDisp x;
  KeyValueDisp y;
  KeyValueDisp z;
  List<Resume> resume;

  Block(
      {this.idService,
      this.idBlock,
      this.duration,
      this.x,
      this.y,
      this.z,
      this.resume});

  factory Block.fromJson(Map<String, dynamic> json) {
    var resume = json['resume'] as List;
    List<Resume> resumenData = resume.map((e) => Resume.fromJson(e)).toList();
    return new Block(
        idService: json['idService'],
        idBlock: json['idBlock'],
        duration: json['duration'],
        x: KeyValueDisp.fromJson(json['x']),
        y: KeyValueDisp.fromJson(json['y']),
        z: KeyValueDisp.fromJson(json['z']),
        resume: resumenData);
  }

}

class Resume {
  String idService;
  int idBlock;
  int duration;
  KeyValueDisp x;
  KeyValueDisp y;
  KeyValueDisp z;

  Resume({this.idService, this.idBlock, this.duration, this.x, this.y, this.z});

  factory Resume.fromJson(Map<String, dynamic> json) {
    return new Resume(
        idService: json['idService'],
        idBlock: json['idBlock'],
        duration: json['duration'],
        x: KeyValueDisp.fromJson(json['x']),
        y: KeyValueDisp.fromJson(json['y']),
        z: KeyValueDisp.fromJson(json['z']));
  }
      Map<String, dynamic> toJson() => resumeToJson(this);

    Map<String, dynamic> resumeToJson(Resume instance) => <String, dynamic>{
      'idService': instance.idService,
      'idBlock': instance.idBlock,
      'duration': instance.duration,
      'x': x.toJson(),
      'y': y.toJson(),
      'z': z.toJson()
    };
}

class KeyValueDisp {
  String init;
  String end;

  KeyValueDisp({this.init, this.end});

  factory KeyValueDisp.fromJson(Map<String, dynamic> json) {
    return new KeyValueDisp(init: json['init'], end: json['end']);
  }

  Map<String, dynamic> toJson() => keyValueToJson(this);

  Map<String, dynamic> keyValueToJson(KeyValueDisp instance) => <String, dynamic>{
    'init': this.init,
    'end': this.end
  };
}

Map<String, dynamic> itemToJson(List<Resume> instance) {
    List<Map<String, dynamic>> resume = instance != null ? instance.map((i) => i.toJson()).toList() : null;

    return <String, dynamic>{
      // Other properties
      'resume': resume,
    };
  }
