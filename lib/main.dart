import 'dart:io';

import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:prueba/blockservice.dart';

//import 'Dart:convert';
void main() => runApp(MaterialApp(home: MyApp()));

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Carwash api',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: DispTurn(),
    );
  }
}

class DispTurn extends StatelessWidget {
  final Map data;
  DispTurn({this.data});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("lista Bloques"),
      ),
      body: FutureBuilder<List<List<Resume>>>(
          future: obtenerdatospost(data),
          builder: (BuildContext context,
              AsyncSnapshot<List<List<Resume>>> snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    List<Resume> block = snapshot.data[index];
                    var snapshot2 = snapshot;
                    return BlockWidget(block, snapshot2);
                  });
            } else if (snapshot.hasError) {
              return Center(
                  child: Text(
                "Error:\n${snapshot.error}",
                style: TextStyle(color: Colors.red),
              ));
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }),
    );
  }
}

class BlockWidget extends StatelessWidget {
  final List<Resume> block;
  final snapshot2;
  BlockWidget(this.block, this.snapshot2);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        String url = "https://api-carwash.herokuapp.com/selectedturn";
       var headers = {HttpHeaders.contentTypeHeader: 'application/json'};
        var data = block;
        var datos = itemToJson(data);
       final posdata = http.post(url, headers: headers, body: json.encode(datos));
       
      },
      child: Card(
        child: Padding(
          padding:
              const EdgeInsets.only(left: 30, top: 20, bottom: 30, right: 30),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                        Text(
                          'Inicio',
                          style: TextStyle(fontSize: 25),
                        )
                      ] +
                      block
                          .map(
                            (b) => Text('${b.y.init}'),
                          )
                          .toList()),
              Container(
                width: 0.5,
                height: 80,
                color: Colors.grey,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [Text('Final', style: TextStyle(fontSize: 25))] +
                    block
                        .map(
                          (b) => Text('${b.y.end}'),
                        )
                        .toList(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}